import pandas as pd
import pymongo
xl_file=pd.read_excel('C:\\Users\\mukiv\\sampledata2.xlsx',0)
category=xl_file['CategoryOrder']
'''Step1:Clean up the Category Special characters (>) and ignore the Home and Add every category in a separate column in the product table itself or separate table for category '''
category=category.str.replace(r'\W',' ',regex=True)
xl_file[['Level0', 'Level 1','Level 2','Level 3','Level 4']] = category.str.split('    ', expand=True)
xl_file.drop(['Level0','CategoryOrder'],axis=1,inplace=True)
xl_file=pd.DataFrame(xl_file,columns=xl_file.columns)
'''Step2:Clean up the Product name by Removing the Brand name'''
for i in xl_file['BrandName']:
    xl_file['ProductName']=xl_file['ProductName'].str.replace(str(i)," ",regex=True)
'''Condition: if the row don’t have Product Name, Ignore the Row (Record details)  before inserting into Database'''
xl_file = xl_file[xl_file.ProductName.notnull()]
xl_file.to_excel("samplepro.xlsx",index=False)
'''Step3:Insert the each row in to product table'''
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb=myclient['SiamProject']
mycollection=mydb['SampleData']
df=pd.read_excel('samplepro.xlsx')
data=df.to_dict(orient='records')
mycollection.insert_many(data)
'''Step4:Retrieve the product count by giving Category Name as parameter to function or file'''
results=list(xl_file[xl_file.columns].count())
for num in range(len(results)):
    print(f"Count based on {xl_file.columns[num]} category is ",results[num])
